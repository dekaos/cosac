<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
if ($_SERVER['SERVER_ADDR'] == 'localhost' || $_SERVER['SERVER_ADDR'] == '192.168.0.120') {
	define('DB_NAME', 'cosac');
	/** Usuário do banco de dados MySQL */
	define('DB_USER', 'root');

	/** Senha do banco de dados MySQL */
	define('DB_PASSWORD', '');

	/** nome do host do MySQL */
	define('DB_HOST', 'localhost');
} else {
	define('DB_NAME', getenv('OPENSHIFT_APP_NAME'));

	/** Usuário do banco de dados MySQL */
	define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));

	/** Senha do banco de dados MySQL */
	define('DB_PASSWORD', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));

	/** nome do host do MySQL */
	define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
}

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eSruk<(Vf4.z&?Q1N*|~06nvbqAZ<+&Zh;*=+zj)R}^.ZBVLJ~S|wgJ])|GCxnX1');
define('SECURE_AUTH_KEY',  'SSo>^YLSa}h _t, I1X+;Gng%+MJs.U!&CMBjA[^|4+tK:niOrw!2a/40.>{4YXK');
define('LOGGED_IN_KEY',    'jKO1J@hI]M%u6B9J_X+We~DD+D81i)h%l3{w1Q5t9,6+0_o;CQW5&+d*2VV+?M*9');
define('NONCE_KEY',        '`GU}k#BW}Dkkm`QD85Z,e+53zs)xb:HdW+1?N :4fqrMtdSw4ki65O+Pu>zpVHQt');
define('AUTH_SALT',        'E}2A+6 <6`|6r5q+7rjuH%KH@e2WRn^+z>RSN-|D|S--pSa/<0Pe_wH,ieldOsPE');
define('SECURE_AUTH_SALT', '8RP#WR+T{_.1)q ,csqN)OA19Zrk=mYP5:>#9J#|vcg. /8k(<{Z`/T~:g5 z`|G');
define('LOGGED_IN_SALT',   'ri|+`e!Oz7rLLFRl?=ZL>d(a^X{TKbXM+[wH(G]#-Abd<Vby9-/r].v,#J`|Z#%3');
define('NONCE_SALT',       '2Mh`lw.f8or7:bm=5E+8*7:Q[kz8aw;FX*X4N]?=d(<szEj[nt<;nvbi VyhD]uz');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'c30ur_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
