<?php

/* Template Name: Blog externo */

$offset = sanitize_text_field($_GET['offset']);

$home_blog_args = array(
  'post_status'    => 'publish',
  'posts_per_page' => 3,
  'category__in'   => array(2, 3),
  'offset'         => $offset
);

$post_data = '';

query_posts($home_blog_args);

while(have_posts()) : the_post();
  $category_name = get_the_category()[0]->name;
  $category_id   = get_cat_ID($category_name);

  $post_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID));

  $post_data .= '<div class="post-inner col-md-4">';
  $post_data .= '<a href="#" class="post-link">';
  $post_data .= '<div class="post-image">';
  $post_data .= '<span class="ajax-loading"><img src="' . get_template_directory_uri() .'/assets/images/ajax-loader.gif" width="16" height="16"></span>';
  $post_data .= '<div class="post-image-inner" style="background: url(' . $post_img . ') no-repeat center center"></div>';
  $post_data .= '</div>';
  $post_data .= '<div class="post-category">' . $category_name . '</div>';
  $post_data .= '<h4 class="post-title">' . get_the_title() . '</h4>';

  if ($category_id == 2) {
    $post_data .= '<div class="post-info">';
    $post_data .= '<span class="post-author">por ' . get_the_author() . '</span> | ';
    $post_data .= '<span class="post-date">' . get_the_time('d.m.Y') . '</span>';
    $post_data .= '</div>';
  }

  $post_data .= '</a>';
  $post_data .= '</div>';
endwhile;

echo $_GET['callback'] . '(' . json_encode(array('post_data' => $post_data)) . ')';
